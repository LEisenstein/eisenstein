﻿using System;

namespace Eisenstein.Utilities
{
    public class EnvironmentSettings
    {
        public static T GetSystemSetting<T>(string settingName)
        {
            var s = System.Environment.GetEnvironmentVariable(settingName, EnvironmentVariableTarget.Machine);

            if (s == null || s.Trim().Length <= 0)
                throw new Exception("Missing Environment Variable.");

            var result = (T)Convert.ChangeType(s, typeof(T));
            return result;
        }

        public static T GetUserSetting<T>(string settingName)
        {
            var s = System.Environment.GetEnvironmentVariable(settingName, EnvironmentVariableTarget.User);
            if (s == null || s.Trim().Length <= 0)
                throw new Exception("Missing Environment Variable.");

            var result = (T)Convert.ChangeType(s, typeof(T));
            return result;
        }

        public static T GetSystemSetting<T>(string settingName, T defaultValue)
        {
            var s = System.Environment.GetEnvironmentVariable(settingName, EnvironmentVariableTarget.Machine);

            if (s == null || s.Trim().Length <= 0)
                return defaultValue;

            var result = (T)Convert.ChangeType(s, typeof(T));
            return result;
        }

        public static T GetUserSetting<T>(string settingName, T defaultValue)
        {
            var s = System.Environment.GetEnvironmentVariable(settingName, EnvironmentVariableTarget.User);
            if (s == null || s.Trim().Length <= 0)
                return defaultValue;

            var result = (T)Convert.ChangeType(s, typeof(T));
            return result;
        }
    }


}
