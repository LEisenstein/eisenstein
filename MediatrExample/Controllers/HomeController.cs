﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using MediatR;
using MediatrExample.Infrastructure.Query;
using MediatrExample.ViewModels;
using MediatrExample.Infrastructure.Command;

namespace MediatrExample.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMediator _mediator;
        public HomeController(IMediator m)
        {
            _mediator = m;
        }



        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Edit()
        {
            HomeEditViewModel vm = new HomeEditViewModel();
            vm.Id = 10;  // Elizabeth Lincoln
            return View(vm);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(HomeEditViewModel model)
        {
            HomeEditCommand command = new HomeEditCommand();
            Mapper.Map(model, command);

            var person = await _mediator.SendAsync(command);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> About()
        {
            HomeAboutQuery query = new HomeAboutQuery();
            query.FirstName = "R";
            var person = await _mediator.SendAsync(query);
            
            ViewData["Message"] = person.FirstName + " " + person.LastName;
            return View();
        }

        public async Task<IActionResult> Contact()
        {
            HomeContactQuery query = new HomeContactQuery();
            query.FirstName = "H";
            var person = await _mediator.SendAsync(query);

            ViewData["Message"] = person.FirstName + " " + person.LastName;
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
