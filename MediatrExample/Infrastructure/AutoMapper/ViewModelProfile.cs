﻿using AutoMapper;
using MediatrExample.Infrastructure.Command;
using MediatrExample.ViewModels;
using MediatrExample.Infrastructure.Entity;

namespace MediatrExample.Infrastructure.AutoMapper
{
    public class ViewModelProfile : Profile
    {
        public ViewModelProfile()
        {
            // CreateMap<>();
            CreateMap<HomeEditCommand, HomeEditViewModel>();
            CreateMap<HomeEditCommand, HomeEditViewModel>().ReverseMap();
            CreateMap<HomeEditCommand, Customer>();
        }

    }
}
