﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using MediatrExample.ViewModels;

namespace MediatrExample.Infrastructure.Command
{
    public class HomeEditCommand : IAsyncRequest<HomeEditViewModel>
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
