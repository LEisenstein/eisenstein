﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MediatrExample.Infrastructure.Entity
{
    public class OrderItem
    {
        public int Id { get; set; }
        public virtual Order Order { get; set; }
        public decimal UnitPrice { get; set; }
        public int Quantity { get; set; }
    }
}
