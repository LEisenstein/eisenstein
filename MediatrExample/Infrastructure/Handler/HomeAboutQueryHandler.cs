﻿using System.Linq;
using System.Threading.Tasks;
using MediatR;
using MediatrExample.Infrastructure.Query;
using MediatrExample.ViewModels;
using Dapper;
using System.Data;

namespace MediatrExample.Infrastructure.Handler
{
    public class HomeAboutQueryHandler : IAsyncRequestHandler<HomeAboutQuery, HomeAboutViewModel>
    {
        // QueryHandlers use Dapper with SqlConnection
        private readonly IDbConnection db;
        public HomeAboutQueryHandler(IDbConnection _db)
        {
            db = _db;
        }

        public async Task<HomeAboutViewModel> Handle(HomeAboutQuery query)
        {
            var sql = "SELECT TOP 1 FirstName, LastName FROM Customer WHERE FirstName LIKE '" + query.FirstName + "%'";
            var result = await db.QueryAsync<HomeAboutViewModel>(sql);

            HomeAboutViewModel model = result.FirstOrDefault();
            return model;
        }
    }
}
