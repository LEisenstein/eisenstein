﻿using MediatR;
using MediatrExample.Infrastructure.Query;
using MediatrExample.ViewModels;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data;

namespace MediatrExample.Infrastructure.Handler
{
    public class HomeContactQueryHandler : IAsyncRequestHandler<HomeContactQuery, HomeContactViewModel>
    {
        // QueryHandlers use Dapper with SqlConnection
        private readonly IDbConnection db;
        public HomeContactQueryHandler(IDbConnection _db)
        {
            db = _db;
        }



        public async Task<HomeContactViewModel> Handle(HomeContactQuery query)
        {
            var sql = "SELECT TOP 1 FirstName, LastName FROM Customer WHERE FirstName LIKE '" + query.FirstName + "%'";
            var result = await db.QueryAsync<HomeContactViewModel>(sql);

            HomeContactViewModel model = result.FirstOrDefault();
            return model;
        }
    }
}
