﻿using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using MediatrExample.ViewModels;
using MediatR;
using System.Data.Entity;
using MediatrExample.Infrastructure.Command;

namespace MediatrExample.Infrastructure.Handler
{
    public class HomeEditCommandHandler : IAsyncRequestHandler<HomeEditCommand, HomeEditViewModel>
    {
        private readonly SampleContext context;
        public HomeEditCommandHandler(SampleContext con)
        {
            context = con;
        }


        public async Task<HomeEditViewModel> Handle(HomeEditCommand command)
        {
            var query = await context.Customers
                                     .Where(x => x.Id == command.Id)
                                     .SingleOrDefaultAsync();

            Mapper.Map(command, query);
            context.SaveChangesAsync();
            HomeEditViewModel vm = new HomeEditViewModel();
            return vm;
        }
    }
}
