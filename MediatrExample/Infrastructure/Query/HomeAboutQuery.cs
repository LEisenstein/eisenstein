﻿using MediatR;
using MediatrExample.ViewModels;

namespace MediatrExample.Infrastructure.Query
{
    public class HomeAboutQuery : IAsyncRequest<HomeAboutViewModel>
    {
        public string FirstName { get; set; }
    }
}
