﻿using MediatR;
using MediatrExample.ViewModels;

namespace MediatrExample.Infrastructure.Query
{
    public class HomeContactQuery : IAsyncRequest<HomeContactViewModel>
    {
        public string FirstName { get; set; }
    }
}
