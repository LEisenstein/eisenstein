﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using MediatrExample.Infrastructure.Entity;

namespace MediatrExample.Infrastructure
{
    [DbConfigurationType(typeof(SampleDbConfiguration))]
    public class SampleContext : DbContext
    {
        public SampleContext(string connectionString) : base(connectionString)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            UseIdValuesGeneratedByTheDatabase(modelBuilder);
            UseSimpleForeignKeyNamingConvention(modelBuilder);
            UseSingularTableNames(modelBuilder);
            SearchThisAssemblyForEntityMappings(modelBuilder);
        }

        private static void UseIdValuesGeneratedByTheDatabase(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add<IdentityConvention>();
        }

        private static void UseSimpleForeignKeyNamingConvention(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Add<ForeignKeyNamingConvention>();
        }

        private static void UseSingularTableNames(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        private static void SearchThisAssemblyForEntityMappings(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.AddFromAssembly(typeof(SampleContext).Assembly);
        }

    } // class
} // namespace
