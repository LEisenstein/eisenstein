﻿using System.Data.Entity;

namespace MediatrExample.Infrastructure
{
    public class SampleDbConfiguration : DbConfiguration
    {
        public SampleDbConfiguration()
        {
            DisableAutomaticDatabaseInitialization();
            SetProviderServices("System.Data.SqlClient", System.Data.Entity.SqlServer.SqlProviderServices.Instance);
        }



        private void DisableAutomaticDatabaseInitialization()
        {
            SetDatabaseInitializer<SampleContext>(null);
        }
    }

    
}
