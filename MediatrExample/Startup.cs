﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using HtmlTags;
using AutoMapper;
using HtmlTags.Conventions;
using MediatR;
using MediatrExample.Infrastructure;
using System.Data.SqlClient;
using System.Data;

namespace MediatrExample
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var MediatrConnectionString = EnvironmentSettings.GetSystemSetting<string>("MediatrExampleDbConnection");
            
            // Add framework services.
            services.AddMvc();

            // AddScoped - Once per Request
            services.AddScoped(_ => new SampleContext(MediatrConnectionString));
            services.AddScoped<IDbConnection, SqlConnection>(_ => new SqlConnection(MediatrConnectionString));

            services.AddHtmlTags(r =>
            {
                r.Defaults();
                // Alaways add BootStrap form-control class to Editors
                r.Editors.Always.AddClass("form-control");
                // Labels convention to always include the CSS classes "control-label" and "col-md-2"
                r.Labels.Always.AddClass("control-label col-md-2");
            });

            services.AddAutoMapper();
            services.AddMediatR(typeof(Startup));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
