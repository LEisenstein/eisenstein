﻿namespace MediatrExample.ViewModels
{
    public class HomeAboutViewModel
    { 
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
