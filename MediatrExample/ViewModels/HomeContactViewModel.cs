﻿namespace MediatrExample.ViewModels
{
    public class HomeContactViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
