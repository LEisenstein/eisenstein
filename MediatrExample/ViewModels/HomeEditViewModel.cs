﻿namespace MediatrExample.ViewModels
{
    public class HomeEditViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
